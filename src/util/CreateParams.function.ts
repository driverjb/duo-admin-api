import { KeyValuePairs } from '../interface';

const replacements = [
  { regex: /!/g, value: '%21' },
  { regex: /'/g, value: '%27' },
  { regex: /\(/g, value: '%28' },
  { regex: /\)/g, value: '%29' },
  { regex: /\*/g, value: '%2A' }
];

/**
 * Module was developed following the instructions provided at:
 * https://duo.com/docs/adminapi#authentication
 */

/**
 * Sort keys according to Duo's specification
 * @param a
 * @param b
 * @returns
 */
function sortKeysLexicographically(a: string, b: string): number {
  for (let i = 0; i < Math.min(a.length, b.length); i++) {
    if (a.charCodeAt(i) > b.charCodeAt(i)) return 1;
    else if (a.charCodeAt(i) < b.charCodeAt(i)) return -1;
  }
  if (a.length > b.length) return 1;
  else if (a.length < b.length) return -1;
  return 0;
}

/**
 * Convert parameters into a querystring.
 * @param params
 * @returns
 */
export function CreateParams(params: KeyValuePairs = {}) {
  const keys = Object.keys(params).sort(sortKeysLexicographically);
  const query = [];
  for (let i = 0; i < keys.length; i++) {
    const left = encodeURIComponent(keys[i]);
    const right = params[keys[i]];
    if (Array.isArray(right))
      for (let j = 0; j < right.length; j++) query.push(`${left}=${encodeURIComponent(right[j])}`);
    else query.push(`${left}=${encodeURIComponent(right)}`);
  }
  let queryString = query.join('&');
  replacements.forEach((r) => (queryString = queryString.replace(r.regex, r.value)));
  return queryString;
}
