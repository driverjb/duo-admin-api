import { Configuration, Method, KeyValuePairs } from '../interface';
import crypto from 'crypto';
import { CreateParams } from './CreateParams.function';

/**
 * Module was developed following the instructions provided at:
 * https://duo.com/docs/adminapi#authentication
 */

/**
 * Sign the request
 * @param config The configuration for the Duo Admin API
 * @param method The HTTP method used for the request
 * @param path The path for the api call
 * @param date The JavaScript Date object
 * @param params The parameters to encode. GET/DELETE will use URLSearchParams, POST uses JSON Body
 * @returns The Basic Auth string to use in the Authentication header
 */
export function CreateBasicAuth(
  config: Configuration,
  method: Method,
  path: string,
  date: string,
  params: KeyValuePairs = {}
) {
  const host = config.api_hostname.toLowerCase();
  const paramString = CreateParams(params);
  const duoString = [date, method.toUpperCase(), host, path, paramString].join('\n');
  const signature = crypto.createHmac('sha1', config.secret_key).update(duoString).digest('hex');
  const basicAuth = Buffer.from(`${config.integration_key}:${signature}`).toString('base64');
  return `Basic ${basicAuth}`;
}
