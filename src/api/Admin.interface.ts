import { Token } from './Token.interface';

/**
 * The administrator's role.
 *
 * One of: "Owner", "Administrator", "Application Manager", "User Manager", "Help Desk", "Billing", "Phishing Manager", or "Read-only".
 *
 * Only present in the response if the customer edition includes the [Administrative Roles](https://duo.com/docs/admin-roles) feature.
 */
export type AdminRole =
  | 'Owner'
  | 'Administrator'
  | 'Application Manager'
  | 'User Manager'
  | 'Help Desk'
  | 'Billing'
  | 'Phishing Manager'
  | 'Read-only';

/**
 * The administrator account's status.
 *
 * |Status|Description|
 * |:--|:--|
 * |_Active_|Admin can log in to Duo.|
 * |_Disabled_|Admin prevented from access.|
 * |_Expired_|Admin blocked from access due to inactivity.|
 * |_Pending Activation_|New admin must complete activation to gain access.|
 */
export type AdminStatus = 'Active' | 'Disabled' | 'Expired' | 'Pending Activation';

/**
 * An abbreviated set of Admin data that is returned along with {@link Token} details.
 */
export interface AdminBrief {
  /** The administrator's ID. */
  admin_id: string;
  /** The administrator's creation date as a UNIX timestamp. No creation date shown for administrators created before October 2021. */
  created: number;
  /** The administrator's email address. */
  email: string;
  /** An integer indicating the last time this administrator logged in, as a Unix timestamp, or null if the administrator has not logged in. */
  last_login: number | null;
  /** The administrator's full name. */
  name: string;
}

/**
 * Details about administrative level users
 */
export interface Admin extends AdminBrief {
  /** The list of administrative units (by admin_unit_id) to which the admin belongs. For an unrestricted admin, this is an empty list. */
  admin_units: string[];
  /** Information about hardware tokens attached to the administrator, or null if none attached. See [Retrieve Hardware Tokens](https://duo.com/docs/adminapi#retrieve-hardware-tokens) for descriptions of the response values. */
  hardtoken: Token | null;
  /** Either true if the administrator must change their password at the next login, or false if no password change is required. */
  password_change_required: boolean;
  /** The administrator's phone number. */
  phone: string;
  /** Is this administrator restricted by an [administrative unit](https://duo.com/docs/adminapi#administrative-units) assignment? Either true or false. Must be set to true in order to [add the admin to an administrative unit using the API](https://duo.com/docs/adminapi#add-administrator-to-administrative-unit). */
  restricted_by_admin_units: boolean;
  /** The administrator's role. See AdminRole type for details. */
  role?: AdminRole;
  /** The administrator account's status. See AdminStatus type for details. */
  status: AdminStatus;
}
