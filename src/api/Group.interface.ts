/**
 * The group's authentication status.
 *
 * |Status|Description|
 * |:--|:--|
 * |_active_|The users in the group must complete secondary authentication.|
 * |_bypass_|The users in the group will bypass secondary authentication after completing primary authentication.|
 * |_disabled_|The users in the group will not be able to authenticate.|
 */
export type GroupStatus = 'active' | 'bypass' | 'disabled';

/**
 * Details about a duo Group
 */
export interface Group {
  /** The group's description. */
  desc: string;
  /** The group's ID. */
  group_id: string;
  /** The group's name. If managed by directory sync, then the name returned here also indicates the source directory. */
  name: string;
  /** The group's authentication status. See GroupStatus type for details. */
  status: GroupStatus;
}

export interface GroupUpdateParameters {
  /** Update the name of the group. */
  name?: string;
  /** The description of the group. */
  desc?: string;
  /** The authentication status of the group. See Retrieve Groups for a list of possible values. See {@link GroupStatus} for details. */
  status?: GroupStatus;
}

export interface GroupCreationParameters extends GroupUpdateParameters {
  /** The name of the group. */
  name: string;
}

export interface MemberListEntry {
  user_id: string;
  username: string;
}
