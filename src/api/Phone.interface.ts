import { Paging } from './Paging.interface';
import { User } from './User.interface';
/**
 * Possible capabilities of the device
 *
 * |Capability|Meaning|
 * |:--|:--|
 * |_auto_|The device is valid for automatic factor selection (e.g. phone or push).|
 * |_push_|The device is activated for Duo Push.|
 * |_phone_|The device can receive phone calls.|
 * |_sms_|The device can receive batches of SMS passcodes.|
 * |_mobile_otp_|The device can generate passcodes with Duo Mobile.|
 */
export type Capability = 'auto' | 'push' | 'phone' | 'sms' | 'mobile_otp';
/**
 * The encryption status of an Android or iOS device file system.
 *
 * One of: "Encrypted", "Unencrypted", or "Unknown". Blank for other platforms.
 *
 * This information is available to [Duo Beyond and Duo Access](https://duo.com/pricing) plan customers.
 */
export type EncryptionStatus = 'Encrypted' | 'Unencrypted' | 'Unknown' | '';
/**
 * Whether an Android or iOS phone is configured for biometric verification.
 *
 * One of: "Configured", "Disabled", or "Unknown". Blank for other platforms.
 *
 * This information is available to [Duo Beyond and Duo Access](https://duo.com/pricing) plan customers.
 */
export type BiometricStatus = 'Configured' | 'Disabled' | 'Unknown' | '';

/**
 * The phone platform. One of: "unknown", "google android", "apple ios", "windows phone 7", "rim blackberry", "java j2me", "palm webos", "symbian os", "windows mobile", or "generic smartphone".
 *
 * "windows phone" is accepted as a synonym for "windows phone 7". This includes devices running Windows Phone 8.
 *
 * If a smartphone's exact platform is unknown but it will have Duo Mobile installed, use "generic smartphone" and generate an activation code. When the phone is activated its platform will be automatically detected.
 */
export type Platform =
  | 'unknown'
  | 'google android'
  | 'apple ios'
  | 'windows phone'
  | 'windows phone 7'
  | 'rim blackberry'
  | 'java j2me'
  | 'palm webos'
  | 'symbian os'
  | 'windows mobile'
  | 'generic smartphone';

/**
 * Whether screen lock is enabled on an Android or iOS phone.
 *
 * One of: "Locked", "Unlocked", or "Unknown". Blank for other platforms.
 *
 * This information is available to [Duo Beyond and Duo Access](https://duo.com/pricing) plan customers.
 */
export type ScreenLockStatus = 'Locked' | 'Unlocked' | 'Unknown' | '';
/**
 * Whether an iOS or Android device is jailbroken or rooted.
 *
 * One of: "Not Tampered", "Tampered", or "Unknown". Blank for other platforms.
 *
 * This information is available to [Duo Beyond and Duo Access](https://duo.com/pricing) plan customers.
 */
export type JailbreakStatus = 'Not Tampered' | 'Tampered' | 'Unknown' | '';
/**
 * The type of phone.
 *
 * One of: "unknown", "mobile", or "landline".
 */
export type DeviceType = 'unknown' | 'mobile' | 'landline';

export interface PhoneBrief {
  /** Has this phone been activated for Duo Mobile yet? Either true or false. */
  activated: boolean;
  /** List of strings, each a factor that can be used with the device. See Capability type for details. */
  capabilities: Capability[];
  /** The encryption status of an Android or iOS device file system. */
  encrypted?: EncryptionStatus;
  /** An extension, if necessary. */
  extension: string;
  /** Whether an Android or iOS phone is configured for biometric verification. */
  fingerprint?: BiometricStatus;
  /** An integer indicating the timestamp of the last contact between Duo's service and the activated Duo Mobile app installed on the phone. Blank if the device has never activated Duo Mobile or if the platform does not support it. */
  last_seen: string;
  /** The phone's model. */
  model: string;
  /** Free-form label for the phone. */
  name: string;
  /** The phone number. A phone with a smartphone platform but no number is a tablet. */
  number: string;
  /** The phone's ID. */
  phone_id: string;
  /** The phone platform. See Platform type for details */
  platform: Platform;
  /** The time (in seconds) to wait after the extension is dialed and before the speaking the prompt. */
  postdelay: string;
  /** The time (in seconds) to wait after the number picks up and before dialing the extension. */
  predelay: string;
  /** Whether screen lock is enabled on an Android or iOS phone. See ScreenLockStatus type for details. */
  screenlock?: ScreenLockStatus;
  /** Have SMS passcodes been sent to this phone? */
  sms_passcodes_sent: boolean;
  /** Whether an iOS or Android device is jailbroken or rooted. See JailbreakStatus type for details. */
  tampered?: JailbreakStatus;
  /** The type of phone. See DeviceType type for details. */
  type: DeviceType;
}

/**
 * A device. Usually this is a phone. However, if the platform is set but has no number, then it's a tablet.
 */
export interface Phone extends PhoneBrief {
  /** A list of users associated with this phone. */
  users: User[];
}

export interface PhoneLookupParameters extends Paging {
  /** Specify a phone number to look up a single phone. */
  number?: string;
  /** The extension, if necessary. */
  extension?: string;
}

export interface PhoneCreateModifyParameters {
  /** The phone number. A phone with a smartphone platform but no number is a tablet. */
  number?: string;
  /** Free-form label for the phone. */
  name?: string;
  /** The extension. */
  extension?: string;
  /** The phone type. See {@link DeviceType} for details. */
  type?: string;
  /** The phone platform. See {@link Platform} for details. */
  platform?: string;
  /** The time (in seconds) to wait after the number picks up and before dialing the extension. */
  predelay?: string;
  /** The time (in seconds) to wait after the extension is dialed and before the speaking the prompt. */
  postdelay?: string;
}

export interface ActivationCodeParameters {
  /**
   * The number of seconds this activation code remains valid. Default: 86400 (one day). Expiration
   * not supported for legacy phone platforms that support passcode generation only (not Duo Push).
   */
  valid_secs?: number;
  /** Specify 1 to also return an installation URL for Duo Mobile; 0 to not return a URL. Default: 0 */
  install?: number;
}

export interface ActivationResponse {
  /** URL of a QR code. Scan the code with Duo Mobile to complete activation. This QR code uses the same activation code as activation_url. */
  activation_barcode: string;
  /** An integer indicating the number of seconds that the activation code remains valid. */
  valid_secs: number;
}

export interface ActivationCodeResponse extends ActivationResponse {
  /** Opening this URL with the Duo Mobile app will complete activation. */
  activation_url: string;
  /** Opening this URL on the phone will prompt the user to install Duo Mobile. Only present if install was 1. */
  installation_url: string;
}

export interface ActivationSmsResponse extends ActivationResponse {
  /** The text of the activation message. */
  activation_msg: string;
  /** The text of the installation message. Only present if the install parameter was set to 1 in the request. */
  installation_msg: string;
}

export interface ActivationParametersSms extends ActivationCodeParameters {
  /** A custom installation message to send to the user. Only valid if installation was requested. Must contain the phrase "<insturl>", which is replaced with the installation URL. */
  installation_msg?: string;
  /** A custom activation message to send to the user. Must contain "<acturl>", which is replaced with the activation URL. */
  activation_msg?: string;
}

export interface InstallationViaSmsResponse {
  /** The text of the installation message. */
  installation_msg: string;
}
