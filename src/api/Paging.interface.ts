export interface Paging {
  /** The maximum number of records returned. Default: 100, Maximum: 300  */
  limit?: number;
  /**
   * The offset from 0 at which to start record retrieval.
   *
   * When used with "limit", the handler will return "limit" records starting at the n-th record,
   * where n is the offset. Default: 0.
   */
  offset?: number;
}
