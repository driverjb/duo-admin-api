import { User } from './User.interface';
import { AdminBrief } from './Admin.interface';
import { Paging } from './Paging.interface';

/**
 * Available token types
 *
 * |Type|Description|
 * |:--|:--|
 * |_h6_|HOTP-6 hardware token|
 * |_h8_|HOTP-8 hardware token|
 * |_yk_|YubiKey AES hardware token|
 * |_d1_|Duo-D100 hardware token|
 */
export type TokenType = 'h6' | 'h8' | 'yk' | 'd1';

export interface TokenBrief {
  /** The serial number of the hardware token; used to uniquely identify the hardware token when paired with type. */
  serial: string;
  /** The hardware token's unique ID. */
  token_id: string;
  /** Value is null for all supported token types. */
  totp_step: null;
  /** The type of hardware token. */
  type: string;
}

/**
 * Duo hardware token
 *
 * Ref: https://duo.com/docs/adminapi#tokens
 */
export interface Token extends TokenBrief {
  /** A list of administrators associated with this hardware token. See Retrieve Administrators for descriptions of the response fields. */
  admins?: AdminBrief[];
  /** A list of end users associated with this hardware token. See Retrieve Users for descriptions of the response fields. */
  users: User[];
}

export interface TokenRetrievalParameters extends Paging {
  /** Specify a type and serial number to look up a single hardware token. See {@link TokenType} for details. */
  type?: TokenType;
  /** The serial number of the hardware token. This option is required if type is present. */
  serial?: string;
}

export interface TokenCreationParameters {
  /** Specify a type and serial number to look up a single hardware token. See {@link TokenType} for details. */
  type: TokenType;
  /** The serial number of the hardware token. This option is required if type is present. */
  serial: string;
  /** The HOTP secret. This parameter is required for HOTP-6 and HOTP-8 hardware tokens. */
  secret?: string;
  /** Initial value for the HOTP counter. This parameter is only valid for HOTP-6 and HOTP-8 hardware tokens. Default: 0. */
  counter?: number;
  /** The YubiKey private ID. This parameter is required for YubiKey hardware tokens. */
  private_id?: string;
  /** The YubiKey AES key. This parameter is required for YubiKey hardware tokens. */
  aes_key?: string;
}

export interface TokenResyncParameters {
  code1: string;
  code2: string;
  code3: string;
}
