import { UserBrief } from './User.interface';

export interface BypassCodeBrief {
  /** The email address of the Duo administrator who created the bypass code. */
  admin_email: string;
  /** The bypass code's identifier. */
  bypass_code_id: string;
  /** The bypass code creation date timestamp. */
  created: number;
  /** An integer indicating the expiration timestamp of the bypass code, or null if the bypass code does not expire on a certain date. */
  expiration: number | null;
  /** An integer indicating the number of times the bypass code may be used before expiring, or null if the bypass code has no limit on the number of times it may be used. */
  reuse_count: number | null;
}

export interface BypassCode extends BypassCodeBrief {
  /** Selected information about the user attached to the bypass code. */
  user: UserBrief;
}
