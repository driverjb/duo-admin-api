import { Group } from './Group.interface';
import { Paging } from './Paging.interface';
import { Phone } from './Phone.interface';
import { Token } from './Token.interface';

/**
 * User status values
 *
 * Note: When a user is a member of a group, the group status may override the individual user's status. Group status is not shown in the user response.
 *
 * |Status|Description|
 * |:--|:--|
 * |_active_|The user must complete secondary authentication.|
 * |_bypass_|The user will bypass secondary authentication after completing primary authentication.|
 * |_disabled_|The user will not be able to log in.|
 * |_locked out_|The user has been automatically locked out due to excessive authentication attempts.|
 * |_pending deletion_|The user was marked for deletion by a Duo admin from the Admin Panel, by the system for inactivity, or by directory sync. If not restored within seven days the user is permanently deleted.|
 */
export type UserStatus = 'active' | 'bypass' | 'disabled' | 'locked out' | 'pending deletion';
export const UserStatusList: UserStatus[] = [
  'active',
  'bypass',
  'disabled',
  'locked out',
  'pending deletion'
];

export interface UserBrief {
  /** The user's username alias(es). Values included for backwards compatibility and reflect the same information as aliases */
  alias1: string | null;
  /** The user's username alias(es). Values included for backwards compatibility and reflect the same information as aliases */
  alias2: string | null;
  /** The user's username alias(es). Values included for backwards compatibility and reflect the same information as aliases */
  alias3: string | null;
  /** The user's username alias(es). Values included for backwards compatibility and reflect the same information as aliases */
  alias4: string | null;
  /** The user's creation date as a UNIX timestamp. */
  created: number;
  /** The user's email address. */
  email: string;
  /** The user's given name. */
  firstname: string;
  /** Is true if the user has a phone, hardware token, WebAuthn security key, or other WebAuthn method available for authentication. Otherwise, false. */
  is_enrolled: boolean;
  /** An integer indicating the last update to the user via directory sync as a Unix timestamp, or null if the user has never synced with an external directory or if the directory that originally created the user has been deleted from Duo. */
  last_directory_sync: number | null;
  /** An integer indicating the last time this user logged in, as a Unix timestamp, or null if the user has not logged in. */
  last_login: number | null;
  /** The user's surname. */
  lastname: string;
  /** Notes about this user. Viewable in the Duo Admin Panel. */
  notes: string;
  /** The user's real name (or full name). */
  realname: string;
  /** The user's status. See {@link UserStatus} type for details. */
  status: UserStatus;
  /** The user's ID. */
  user_id: string;
  /** The user's username. */
  username: string;
}

/**
 * Duo user
 *
 * Ref: https://duo.com/docs/adminapi#users
 */
export interface User extends UserBrief {
  /** Map of the user's username alias(es). Up to eight aliases may exist. */
  aliases: {
    alias1?: string;
    alias2?: string;
    alias3?: string;
    alias4?: string;
    alias5?: string;
    alias6?: string;
    alias7?: string;
    alias8?: string;
  };
  /** List of groups to which this user belongs. See [Retrieve Groups](https://duo.com/docs/adminapi#retrieve-groups) for response info. */
  groups: Group[];
  /**  A list of phones that this user can use. See [Retrieve Phones](https://duo.com/docs/adminapi#retrieve-phones) for descriptions of the phone response values. */
  phones: Phone[];
  /** A list of tokens that this user can use. See [Retrieve Hardware](https://duo.com/docs/adminapi#retrieve-hardware-tokens) Tokens for descriptions of the response values. */
  tokens: Token[];
  /** A list of WebAuthn authenticators that this user can use. See [Retrieve WebAuthn Credentials by User ID](https://duo.com/docs/adminapi#retrieve-webauthn-credentials-by-user-id) for descriptions of the response values. */
  webauthncredentials: any[];
}

/**
 * Parameters available when searching for user data
 */
export interface UserLookupParameters extends Paging {
  /**
   * Specify a username (or username alias) to look up a single user.
   */
  username?: string;
}

export interface UserModificationParameters {
  /** The new username. */
  username?: string;
  /** Username aliases for the user. Up to eight aliases may be specified. */
  aliases?: string[];
  /** The real name (or full name) of this user. */
  realname?: string;
  /** The email address of this user. */
  email?: string;
  /** The user's status. See {@link UserStatus} for details. Status values 'locked out' and 'pending deletion' are not allowed here.*/
  status?: UserStatus;
  /** An optional description or notes field. Can be viewed in the Duo Admin Panel. */
  notes?: string;
  /** The user's given name. */
  firstname?: string;
  /** The user's surname. */
  lastname?: string;
}

export interface UserCreationParameters extends UserModificationParameters {
  /** The name of the user to create. */
  username: string;
}

export interface UserEnrollmentParameters {
  /** The user name (or username alias) of the user to enroll. */
  username: string;
  /** The email address of this user. */
  email: string;
  /** The number of seconds the enrollment code should remain valid. Default: 2592000 (30 days). */
  valid_secs?: number;
}

export interface UserCreateBypassCodes {
  /**
   * Number of new bypass codes to create. At most 10 codes (the default) can be created at a time.
   * Codes will be generated randomly.
   */
  count?: number;
  /** CSV string of codes to use. Mutually exclusive with count. Maximum: 100 */
  codes?: string[];
  /**
   * The number of times generated bypass codes can be used. If 0, the codes will have an infinite
   * reuse_count. Default: 1.
   */
  reuse_count?: number;
  /**
   * The number of seconds for which generated bypass codes remain valid. If 0 (the default) the
   * codes will never expire.
   */
  valid_secs?: number;
}

export interface DirectorySyncResponse {
  message: string;
  user: User;
}
