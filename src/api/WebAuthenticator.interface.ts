/**
 * Indicates the type of WebAuthn credential
 *
 * One of: "Security Key" or "Touch ID".
 */
export type WebAuthLabel = 'Security Key' | 'Touch ID';

/**
 * Web authentication details
 */
export interface WebAuthenticator {
  /** Free-form label for the WebAuthn credential. */
  credential_name: string;
  /** The date the WebAuthn credential was registered in Duo. */
  date_added: number;
  /** Indicates the type of WebAuthn credential. See WebAuthLabel type for details. */
  label: WebAuthLabel;
  /** The WebAuthn credential's registration identifier. */
  webauthnkey: string;
}
