export interface Configuration {
  /** The administrative hostname url provided by Duo */
  api_hostname: string;
  /** The integration key provided when you create an application to protect */
  integration_key: string;
  /** The secret key that is provided when you create an application to protect. This is only visible at the moment of creation... don't lose it. */
  secret_key: string;
}
