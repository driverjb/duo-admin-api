export type Result = 'OK' | 'FAIL';

export interface DuoResponseJson {
  /** The overall result of the call. This will be OK or FAIL */
  stat: Result;
}

export interface Metadata {
  next_offset: number;
  prev_offset: number;
  total_objects: number;
}

/**
 * Successful response format. See https://duo.com/docs/adminapi#response-format for details.
 */
export interface DuoResponseSuccessJson<T> extends DuoResponseJson {
  /** Data that will be included when paging of results needs to occur */
  metadata?: Metadata;
  /** The data returned by the API call */
  response: T;
}

/**
 * Error response format. See https://duo.com/docs/adminapi#response-format for details.
 */
export interface DuoResponseFailJson extends DuoResponseJson {
  /** The error code returned from the Duo API. */
  code: number;
  message: string;
  message_detail: string;
}
