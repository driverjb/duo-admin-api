export type Method = 'get' | 'post' | 'delete' | 'put';
export interface KeyValuePairs {
  [key: string]: any;
}
