import {
  Configuration,
  Method,
  DuoResponseJson,
  DuoResponseSuccessJson,
  KeyValuePairs
} from '../interface';
import { DuoError } from '.';
import { CreateBasicAuth } from '../util';
import { UserAPI } from './UserAPI.class';
import { GroupAPI } from './GroupAPI.class';
import { PhoneAPI } from './PhoneAPI.class';
import { TokenAPI } from './TokenAPI.class';
import https from 'https';
import { CreateParams } from '../util/CreateParams.function';
import { readFileSync } from 'fs';
import { resolve } from 'path';

const PACKAGE: any = JSON.parse(readFileSync(resolve('package.json')).toString()).version;
const PACKAGE_VERSION = PACKAGE.version;
const PACKAGE_NAME = PACKAGE.name;

/**
 * The main class which uses a provided configuration to interact with the Duo administrative API.
 * See the [Admin Guide](https://duo.com/docs/adminapi#overview) for help understanding how to
 * retrieve the desired data as well as how to handle paging result sets that contain large amounts
 * of data.
 */
export class DuoAPI {
  /**
   * All user functions detailed in the Duo Admin API reference document.
   *
   * https://duo.com/docs/adminapi#users
   */
  readonly user: UserAPI;
  /**
   * All group functions detailed in the Duo Admin API reference document.
   *
   * https://duo.com/docs/adminapi#groups
   */
  readonly group: GroupAPI;
  /**
   * All phone functions detailed in the Duo Admin API reference document.
   *
   * https://duo.com/docs/adminapi#phones
   */
  readonly phone: PhoneAPI;
  /**
   * All token functions detailed in the Duo Admin API reference document.
   *
   * https://duo.com/docs/adminapi#tokens
   */
  readonly token: TokenAPI;
  constructor(private conf: Configuration) {
    this.user = new UserAPI(this);
    this.group = new GroupAPI(this);
    this.phone = new PhoneAPI(this);
    this.token = new TokenAPI(this);
  }
  private getDate() {
    return new Date().toUTCString();
  }
  /**
   * Prepare the options for sending an https request
   * @param path The path for the duo api request
   * @param method HTTP Method used to make the request
   * @param params Key/Value pairs that will be used in the request (body/query will be automatically determined)
   * @returns A Json object representing the options for a request with the given parameters
   */
  private getRequestOptions(path: string, method: Method, params: KeyValuePairs = {}) {
    const date = this.getDate();
    const headers: any = {};
    const host = this.conf.api_hostname;
    headers.Date = date;
    headers.Authorization = CreateBasicAuth(this.conf, method, path, date, params);
    headers.Host = host;
    headers['User-Agent'] = `${PACKAGE_NAME}/${PACKAGE_VERSION}`;
    if (method === 'post' || method === 'put')
      headers['Content-Type'] = 'application/x-www-form-urlencoded';
    else path = `${path}?${CreateParams(params)}`;
    return { headers, host, path, method };
  }
  /**
   * Send a request to the Duo Admin API
   * @param path The path for the duo api request
   * @param method HTTP Method used to make the request
   * @param params Key/Value pairs that will be used in the request (body/query will be automatically determined)
   * @returns A Json object retrieved from the API. Use the provided interfaces to provide type information for the results.
   */
  public request<T>(path: string, method: Method, params: KeyValuePairs = {}) {
    const options = this.getRequestOptions(path, method, params);
    let body = '';
    if (method === 'post' || method === 'put') body = CreateParams(params);
    return new Promise<DuoResponseSuccessJson<T>>((resolve, reject) => {
      const request = https.request(options, (res) => {
        const data: string[] = [];
        res.setEncoding('utf-8');
        res.on('error', reject);
        res.on('data', (chunk) => data.push(chunk));
        res.on('end', () => {
          const json = JSON.parse(data.join(''));
          if ((json as DuoResponseJson).stat === 'FAIL') reject(new DuoError(json));
          else resolve(json as DuoResponseSuccessJson<T>);
        });
      });
      request.write(body);
      request.on('error', reject);
      request.end();
    });
  }
}
