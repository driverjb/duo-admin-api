import * as API from '../api';
import { DuoAPI } from './DuoApi.class';

export class TokenAPI {
  constructor(private duo: DuoAPI) {}
  /**
   * Returns a paged list of OTP hardware tokens. To fetch all results, call repeatedly with the
   * offset parameter as long as the result metadata has a next_offset value. If no type and serial
   * parameters are provided, the list will contain all hardware tokens. Otherwise, the list will
   * contain either a single hardware token (if a match was found) or no hardware tokens. Requires
   * "Grant read resource" API permission.
   *
   * https://duo.com/docs/adminapi#retrieve-hardware-tokens
   * @param args
   * @returns A list of hardware tokens
   */
  retrieve(args?: API.TokenRetrievalParameters) {
    return this.duo.request<API.Token[]>('/admin/v1/tokens', 'get', args);
  }

  /**
   * Create a new hardware token. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#create-hardware-token
   * @param args
   * @returns the newly created hardware token
   */
  create(args: API.TokenCreationParameters) {
    return this.duo.request<API.Token>('/admin/v1/tokens', 'post', args);
  }

  /**
   * Return the single hardware token with token_id. Requires "Grant read resource" API permission.
   *
   * https://duo.com/docs/adminapi#retrieve-hardware-token-by-id
   * @param token_id
   * @returns hardware token object
   */
  retrieveById(token_id: string) {
    return this.duo.request<API.Token>(`/admin/v1/tokens/${token_id}`, 'get');
  }

  /**
   * Resynchronize the hardware token with ID token_id by providing three successive codes from the
   * token. Only HOTP and Duo-D100 tokens can be resynchronized. YubiKey tokens operating in their
   * native AES mode do not need resynchronization. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#resync-hardware-token
   * @param token_id
   * @param args
   * @returns empty string
   */
  resync(token_id: string, args: API.TokenResyncParameters) {
    return this.duo.request<string>(`/admin/v1/tokens/${token_id}/resync`, 'post', args);
  }

  /**
   * Delete the hardware token with ID token_id from the system. Requires "Grant write resource"
   * API permission.
   *
   * https://duo.com/docs/adminapi#delete-hardware-token
   * @param token_id
   * @returns empty string
   */
  delete(token_id: string) {
    return this.duo.request<string>(`/admin/v1/tokens/${token_id}`, 'post');
  }
}
