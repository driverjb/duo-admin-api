import * as API from '../api';
import { DuoAPI } from './DuoApi.class';

export class PhoneAPI {
  constructor(private duo: DuoAPI) {}
  /**
   * Returns a paged list of phones. To fetch all results, call repeatedly with the offset parameter
   * as long as the result metadata has a next_offset value. If no number or extension parameters
   * are provided, the list will contain all phones. Otherwise, the list will contain either single
   * phone (if a match was found), or no phones. Requires "Grant read resource" API permission.
   *
   * https://duo.com/docs/adminapi#retrieve-phones
   * @param args
   * @returns a list of phones (even if it's only one specified by a phone number)
   */
  retrieve(args?: API.PhoneLookupParameters) {
    return this.duo.request<API.Phone[]>('/admin/v1/phones', 'get', args);
  }
  /**
   * Create a new phone with a specified phone number or other parameters. Requires "Grant write
   * resource" API permission.
   *
   * https://duo.com/docs/adminapi#create-phone
   * @param args
   * @returns the newly created phone object
   */
  create(args?: API.PhoneCreateModifyParameters) {
    return this.duo.request<API.Phone>('/admin/v1/phones', 'post', args);
  }
  /**
   * Return the single phone with phone_id. Requires "Grant read resource" API permission.
   *
   * https://duo.com/docs/adminapi#retrieve-phone-by-id
   * @param phone_id
   * @returns a phone object
   */
  retrieveById(phone_id: string) {
    return this.duo.request<API.Phone[]>(`/admin/v1/phones/${phone_id}`, 'get');
  }
  /**
   * Change the details of the phone with ID phone_id. Requires "Grant write resource" API
   * permission.
   *
   * https://duo.com/docs/adminapi#modify-phone
   * @param phone_id
   * @returns the newly modified phone object
   */
  modify(phone_id: string, args: API.PhoneCreateModifyParameters) {
    return this.duo.request<API.Phone[]>(`/admin/v1/phones/${phone_id}`, 'post', args);
  }

  /**
   * Delete the phone with ID phone_id from the system. Requires "Grant write resource" API
   * permission.
   *
   * https://duo.com/docs/adminapi#delete-phone
   * @param phone_id
   * @returns Empty string
   */
  delete(phone_id: string) {
    return this.duo.request<string>(`/admin/v1/phones/${phone_id}`, 'delete');
  }

  /**
   * Generate a Duo Mobile activation code. This method will fail if the phone's type or platform
   * are Unknown. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#create-activation-code
   * @param phone_id
   * @param args
   * @returns Details about the activation code
   */
  createActivationCode(phone_id: string, args: API.ActivationCodeParameters) {
    return this.duo.request<API.ActivationCodeResponse>(
      `/admin/v1/phones/${phone_id}/activation_url`,
      'post',
      args
    );
  }

  /**
   * Generate a Duo Mobile activation code and send it to the phone via SMS, optionally sending an
   * additional message with a URL to install Duo Mobile. This method will fail if the phone's type
   * or platform are Unknown. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#send-activation-code-via-sms
   * @param phone_id
   * @param args
   * @returns
   */
  sendActivationCodeViaSms(phone_id: string, args: API.ActivationParametersSms) {
    return this.duo.request<API.ActivationSmsResponse>(
      `/admin/v1/phones/${phone_id}/send_sms_activation`,
      'post',
      args
    );
  }

  /**
   * Send a message via SMS describing how to install Duo Mobile. This method will fail if the
   * phone's type or platform are Unknown. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#send-installation-url-via-sms
   * @param phone_id
   * @param installation_message
   * @returns The message that was delivered to the recipient
   */
  sendInstallationUrlViaSms(phone_id: string, installation_message?: string) {
    return this.duo.request<API.ActivationSmsResponse>(
      `/admin/v1/phones/${phone_id}/send_sms_installation`,
      'post',
      { installation_message }
    );
  }

  /**
   * Generate a new batch of SMS passcodes send them to the phone in a single SMS message. Requires
   * "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#send-passcodes-via-sms
   * @param phone_id
   * @returns Empty string
   */
  sendPasscodesViaSms(phone_id: string) {
    return this.duo.request<string>(`/admin/v1/phones/${phone_id}/send_sms_passcodes`, 'post');
  }
}
