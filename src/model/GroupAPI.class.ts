import * as API from '../api';
import { DuoAPI } from './DuoApi.class';

export class GroupAPI {
  constructor(private duo: DuoAPI) {}
  /**
   * Returns a paged list of groups. To fetch all results, call repeatedly with the offset parameter
   * as long as the result metadata has a next_offset value. Requires "Grant read resource" API
   * permission.
   *
   * https://duo.com/docs/adminapi#retrieve-groups
   * @param args
   * @returns A list of group objects
   */
  retrieve(args?: API.Paging) {
    return this.duo.request<API.Group[]>('/admin/v1/groups', 'get', args);
  }
  /**
   * Create a new group. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#create-group
   * @param args
   * @returns the newly created group
   */
  create(args: API.GroupCreationParameters) {
    return this.duo.request<API.Group>('/admin/v1/groups', 'post', args);
  }
  /**
   * Retrieve information about a group. Note that this output does not include a list of group
   * members.
   *
   * https://duo.com/docs/adminapi#get-group-info
   * @param group_id
   * @returns A group object excluding its members
   */
  getInfo(group_id: string) {
    return this.duo.request<API.Group>(`/admin/v2/groups/${group_id}`, 'get');
  }

  /**
   * Returns a paged list of members of a specified group. To fetch all results, call repeatedly
   * with the offset parameter as long as the result metadata has a next_offset value.
   *
   * https://duo.com/docs/adminapi#v2-groups-get-users
   * @param group_id
   * @returns a list of users (limited to user_id and username)
   */
  getMembers(group_id: string, args?: API.Paging) {
    return this.duo.request<API.MemberListEntry[]>(
      `/admin/v2/groups/${group_id}/users`,
      'get',
      args
    );
  }

  /**
   * Update information about a group. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#update-group
   * @param group_id
   * @param args
   * @returns The newly updated group
   */
  update(group_id: string, args: API.GroupUpdateParameters) {
    return this.duo.request<API.Group>(`/admin/v1/groups/${group_id}`, 'post', args);
  }

  /**
   * Delete a group. Requires "Grant write resource" API permission.
   *
   * https://duo.com/docs/adminapi#delete-group
   * @param group_id
   * @returns Empty string
   */
  delete(group_id: string) {
    return this.duo.request<string>(`/admin/v1/groups/${group_id}`, 'delete');
  }
}
