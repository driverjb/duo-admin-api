import { DuoResponseFailJson } from '../interface';

/**
 * An object representing a Duo API Error response
 */
export class DuoError {
  constructor(private raw_json: DuoResponseFailJson) {}
  /**
   * Error code used to lookup information here https://duo.com/docs/adminapi#response-format
   */
  public get Code() {
    return this.raw_json.code;
  }
  /**
   * Information describing the problem
   */
  public get Message() {
    return this.raw_json.message;
  }
  /**
   * Details provided to help understand the Message
   */
  public get Detail() {
    return this.raw_json.message_detail ?? 'No details provided';
  }
  /**
   * Return the HTTPCode related to this Error
   */
  public get HTTPCode() {
    return Number(this.Code.toString().slice(0, 3));
  }
  /**
   * Generate a string representing the error. Great for log entries.
   * @returns
   */
  public toString() {
    return `DuoError[${this.Code}]: ${this.Message} - ${this.Detail}`;
  }
  /**
   * Generate a clean representation of the object in JSON format
   * @returns
   */
  public toJson() {
    return {
      Code: this.Code,
      Message: this.Message,
      Detail: this.Detail
    };
  }
}
