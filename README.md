# Duo Admin API

Open source implementation of the Duo Admin API. The officially supported [module](https://www.npmjs.com/package/@duosecurity/duo_api) contains deprecated code and my [pull requests](https://github.com/duosecurity/duo_api_nodejs/pull/31) have been ignored. Because of this, I decided to create and support my own for as long as I need it.

Some enhancements that you won't find in the original include:

- Typescript support
- Defined interfaces for returned data and request parameters
- Documentation snippets straight from the source in the intellisense comments along with the direct
  link to the exact api documentation.
- Promise support

# Installation and Setup

Use npm to install the package:

```
npm install --save duo-admin-api
```

After installing, you'll need to login to your Duo Admin portal and generate the required keys called the integration_key and the secret_key.

# Usage

Include the exported class definition, DuoAPI. Create an instance by feeding in your admin hostname, integration_key, and secret_key then you are ready to make any calls you need.

```typescript
import { DuoAPI, DuoError } from './index';

const config = {
  api_hostname: 'api-somesupersecreturl.duosecurity.com',
  integration_key: 'supersecret_i_key',
  secret_key: 'somesuperduperlong_s_key'
};

const dapi = new DuoAPI(config);

dapi.user
  .retrieve({ username: 'someguy' })
  .then((result) => {
    console.log(result.response); //an array of users... which should just be one if someguy exists, empty array if not
  })
  .catch((error: DuoError) => {
    console.log(error.toString()); //a fancy error that interprets and nicely displays the duo response for stat == FAIL
  });
```

# Detailed Usage Notes

The Duo Admin API always responds to requests in a standard format. Every response will include the
field _stat_. This field will be a string that has one of two values: `OK` or `FAIL`. Depending
on which of these values you receive the rest of the response will be different.

These responses are described in detail here: https://duo.com/docs/adminapi#api-details.

### OK

When the _stat_ value is `OK` there will be one or two additional fields returned. You will always see the _response_ field. This field includes the data that was returned in response to the API call that was made. If the call has the ability to return large amounts of data, then you will also see the _metadata_ field. This field assists you with making additional calls to retreive more of the paged data you are after. Please see Duo's documentation for more details about paging: https://duo.com/docs/adminapi#response-paging.

### FAIL

When the _stat_ value is `FAIL` there will always be three additional fields:

1. **code**: Provides an error code that can be used to find more information in Duo's documentation.
1. **message**: Provides a brief message describing the problem with the API call.
1. **message_detail**: Provides additional details to help understand the problem with the API call.

# Current API Feature Implementation

The full documentation for the Duo Admin API is publicly available on their site: https://duo.com/docs/adminapi. I do not currently need the additional functionality provided by the remaining API modules. However, I welcome merge requests as long as they are built in the same style and contain the same documentation that my existing code contains. If this package begins to see a lot of use, then I might add the remaining modules. Until then, here's the current state:

- [x] Users
- [x] Groups
- [x] Phones
- [x] Tokens
- [ ] WebAuthn Credentials
- [ ] Bypass Codes
- [ ] Integrations
- [ ] Endpoints
- [ ] Administrators
- [ ] Administrative Units
- [ ] Logs
- [ ] Trust Monitor
- [ ] Settings
- [ ] Custom Branding
- [ ] Account Info

While I don't have direct calls for the remaining API endpoints, I did leave the "request" method exposed which allows you to manually input any path, method, and set of parameters you'd like to use straight from the [Duo Admin API Documentation](https://duo.com/docs/adminapi).

If you like this module, or want to prod me into adding more features, feel free to throw a donation my way using my personal PayPal link: https://paypal.me/driverjb
